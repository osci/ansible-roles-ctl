UNRELEASED
----------

- fix bogus HEAD branch accidentally created when target is 'HEAD'
- add a 'fix' subcommand to repair the bogus HEAD and possible other problems
  in the future

v2.0.0
------

- add collections support
- version now defaults to HEAD, the repo's default, instead of forcing it to
  master
- update does not change the requirements anymore (and therefore does not update
  the requirement file): the feature has been broken for a while, noone noticed
  and it is too fragile anyway. The requirement file is not difficult to
  maintain manually anyway.
- using a tag as version was broken
- update can now properly switch to/from branches/tags
- reference existence was not checked in all situations
- missing dependency on pyyaml

v1.2.0
------

- Add completion
- Fix crash because keys() now returns an iterator
- Workaround argparse problem with choices and 0 arguments

v1.1.0
------

- remove use of Exception.message that was deprecated and then removed
- ignore leftover from Galaxy installation
- warn when tracking the wrong origin

v1.0.2
------

- fix missing import

v1.0.1
------

- fix buildsys to properly build the wheel with pip

v1.0.0
------

- Initial release.

