# Ansible roles management

The goal of this tool is to manage installation and upgrade of Ansible
roles and collections using git tags and branches.

This tool uses the `requirements.yml` configuration file used by Ansible
Galaxy. The syntax is unchanged and the ansible-galaxy tool can be used
by people not involved in playbooks development. Version 1 of the requirements
file with only a list of roles is still supported, but if you need collections
then you need to convert it to version 2 (containing a list of 'roles' and a
list of 'collections').

Support for collections is limited to installing a full repo with all included
collections. This means all included collections (if many) will be enabled and
you cannot select to enable just some of them. The name of the collection must
contain the namespace to be installed in the proper path (as defined in Ansible
documentation). For example:

```
roles:
…
collections:
  - name: kubernetes.core
    type: git
    source: https://github.com/ansible-collections/kubernetes.core.git
    version: stable-2.2
```

will install it at `<collections_path>/ansible_collections/kubernetes/core/` with
`collections_path` defined in your `ansible.cfg` or using Ansible default.

This tool assumes a **role/collection version** follows [semantic versioning](http://semver.org/spec/v2.0.0.html)
and is labelled with a git tag of the form _vX.Y_ or _vX.Y.Z_ (examples: v1.0 or v2.3.1). In fact the _v_ is optional but recommended for clarity.

Roles managed manually, bare, or in a bad shape are ignored (with proper
warning).

## Installation

You can install it using PyPI:

    pip install ansible-roles-ctl

Or you can run it in-place since it has very few dependencies (you only
need the ansible-roles-ctl script, version.py is only used in the build
process).

You need the following dependencies:

- Ansible (probably starting from 2.7)
- [GitPython](https://gitpython.readthedocs.io/) (package 'python3-git' on Debian systems, or using pip)
- [argcomplete](https://kislyuk.github.io/argcomplete/) >= 1.9.2 (package 'python3-argcomplete' on Debian systems, or using pip)

## Completion

Command completion is done using [argcomplete](https://github.com/kislyuk/argcomplete).
It needs to be enabled first to work. The easiest way is to use the
`activate-global-python-argcomplete3` script as root. Other methods are
described on the `argcomplete` website.

## Usage

Syntax is as follow:

    ansible-roles-ctl [global options] [subcommand] [subcommand options]

You can use -h/--help option to list all available options at command
and subcommand level, as well as all available subcommands.

Follows documentation about the various subcommands.

### Status

    ansible-roles-ctl status

This subcommand display a status report about all required roles/collections.

You can limit output to a selection of roles/collections:

    ansible-roles-ctl status mailman3 httpd

The -c/--changelog option displays changelog entries if available.

### Install

    ansible-roles-ctl install

This subcommand install all roles/collections listed in the requirements.

You can limit this action to a selection of roles/collections:

    ansible-roles-ctl install bind9 postfix

### Update

    ansible-roles-ctl update

This subcommand update all roles/collections listed in the requirements to the
latest version.

You can limit this action to a selection of roles/collections:

    ansible-roles-ctl update bind9 postfix
